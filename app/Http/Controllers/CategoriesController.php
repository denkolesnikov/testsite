<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function show(Category $category){

        $data['products'] = $category->products;
        $data['categories'] = Category::all();
        return view('homepage', $data);
    }
}
