<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index(){

        $data['categories'] = Category::all();
        return view('admin.categories.index', $data);

    }

    public function show(Category $category){

        return view('admin.categories.show', compact('category'));

    }

    public function create(){
        return view('admin.categories.create');
    }

    public function store(){


        $this->validate(request(),[
            'title' => 'required|min:3',
            'alias' => 'required|min:4|max:20|unique:categories,alias',
        ]);

        Category::create(request(['title','alias']));

        return redirect('/admin/categories');
    }

    public function edit(Category $category){
        return view('admin.categories.edit', compact('category'));
    }

    public function update(Category $category){
        $this->validate(request(),[
            'title' => 'required|min:3',
            'alias' => 'required|min:4|max:20|unique:products,alias,'.$category->id,
            'price' => 'required|numeric',
            'description'  => 'min:20',
        ]);

        $category->update(request(['title','alias']));

        return redirect('/admin/categories');
    }

    public function delete(Category $category){
        return view('admin.categories.delete', compact('category'));
    }

    public function destroy(Category $category){
        $category->delete();
        return redirect('/admin/categories');
    }
}
