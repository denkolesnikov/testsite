<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{

    public function index(){

        $data['pages'] = Page::all();
        return view('admin.pages.index', $data);

    }

    public function show(Page $page){

        return view('pages.show', compact('page'));

    }

    public function create(){
        return view('admin.pages.create');
    }

    public function store(){


        $this->validate(request(),[
            'title' => 'required|min:3',
            'alias' => 'required|min:4|max:20|unique:products,alias',
            'intro' => 'required|min:10',
            'description'  => 'min:30',
        ]);

        Page::create(request(['title','alias','intro','content']));

        return redirect('/admin/pages');
    }

    public function edit(Page $page){
        return view('admin.pages.edit', compact('page'));
    }

    public function update(Page $page){
        $this->validate(request(),[
            'title' => 'required|min:3',
            'alias' => 'required|min:4|max:20|unique:products,alias,'.$page->id,
            'intro' => 'required|min:10',
            'content'  => 'min:30',
        ]);

        $page->update(request(['title','alias','intro','content']));

        return redirect('/admin/pages');
    }

    public function delete(Page $page){
        return view('admin.pages.delete', compact('page'));
    }

    public function destroy(Page $page){
        $page->delete();
        return redirect('/admin/pages');
    }
}
