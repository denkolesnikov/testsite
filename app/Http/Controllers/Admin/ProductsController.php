<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{

    public function showCategory(Category $category){

        $data['products'] = $category->products;
        $data['categories'] = Category::all();
        return view('admin.products.index', $data);

    }


    public function index(){

        $data['products'] = Product::all();
        $data['categories'] = Category::all();
        return view('admin.products.index', $data);

    }

    public function show(Product $product){

        return view('admin.products.show', compact('product'));

    }

    public function create(){
        $data['categories'] = Category::all();
        return view('admin.products.create', $data);
    }

    public function store(){


        $this->validate(request(),[
            'title' => 'required|min:3',
            'alias' => 'required|min:4|max:20|unique:products,alias',
            'category_id'=>'required|numeric',
            'price' => 'required|numeric',
            'description'  => 'min:20',
        ]);

        Product::create(request(['title','alias','category_id','price','description']));

        return redirect('/admin/products');
    }

    public function edit(Product $product){
        $data['product'] = $product;
        $data['categories'] = Category::all();
        return view('admin.products.edit', $data);
    }

    public function update(Product $product){
        $this->validate(request(),[
            'title' => 'required|min:3',
            'alias' => 'required|min:4|max:20|unique:products,alias,'.$product->id,
            'price' => 'required|numeric',
            'description'  => 'min:20',
        ]);

        $product->update(request(['title','alias','price','description']));

        return redirect('/admin/products');
    }

    public function delete(Product $product){
        return view('admin.products.delete', compact('product'));
    }

    public function destroy(Product $product){
        $product->delete();
        return redirect('/admin/products');
    }
}
