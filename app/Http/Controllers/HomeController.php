<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){

        $data['products'] = Product::all();
        $data['categories'] = Category::all();
        return view('homepage', $data);

    }
}
