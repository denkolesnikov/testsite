<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['title','alias','category_id','price','description'];

    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function category(){

        return $this->belongsTo(Category::class);
    }
}
