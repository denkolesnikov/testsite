<?php

Route::get('/', 'HomeController@home');

Route::get('/login', 'SessionsController@create')->name('login');

Route::get('/logout', 'SessionsController@destroy');

Route::post('/sessions', 'SessionsController@store');

Route::get('/register', 'RegistrationController@create');

Route::post('/register', 'RegistrationController@store');


Route::get('/categories/{category}', 'CategoriesController@show');

Route::get('/pages', 'PagesController@index');

Route::get('/pages/{page}', 'PagesController@show');

Route::get('/products/{product}', 'ProductsController@show');


Route::group(['middleware' => 'auth', 'namespace' => 'Admin' ,'prefix' => 'admin'], function()
{

    Route::get('/', 'MainController@index');

    Route::get('/categories/{category}/delete', 'CategoriesController@delete');

    Route::get('/products/{product}/delete', 'ProductsController@delete');

    Route::get('/products/category/{category}', 'ProductsController@showCategory');

    Route::get('/pages/{page}/delete', 'PagesController@delete');


    Route::resources([
        'products' => 'ProductsController',
        'pages' => 'PagesController',
        'categories' => 'CategoriesController'
    ]);

});

