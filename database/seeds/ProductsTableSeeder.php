<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => 'Sony Xperia XZ1',
                'alias' => 'sony-xperia-XZ1',
                'price' => 566.98,
                'description' => 'This cool phone Sony Xperia XZ1 for you',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Xiaomi Redmi',
                'alias' => 'xiaomi-redmi',
                'price' => 115.45,
                'description' => 'This cool phone Xiaomi Redmi for you',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Asus VivoBook Max',
                'alias' => 'asus-vivobook-max',
                'price' => 343.45,
                'description' => 'This cool laptop Asus VivoBook Max for you',
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
