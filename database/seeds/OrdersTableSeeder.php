<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'customer_name' => 'Edvard Karen',
                'email' => 'edvard2134@gmail.com',
                'phone' => '+380483601656',
                'feedback' => 'Very cool device. I recommend 12',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'customer_name' => 'Jaime Jackson',
                'email' => 'jaimejack@gmail.com',
                'phone' => '+380489500463',
                'feedback' => 'Very cool device. I recommend 43',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'customer_name' => 'Kathryn	Myers',
                'email' => 'kathryn23@gmail.com',
                'phone' => '+380482442284',
                'feedback' => 'Very cool device. I recommend 75',
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
