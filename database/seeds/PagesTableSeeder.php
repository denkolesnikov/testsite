<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'title' => 'Privacy Policy',
                'alias' => 'privacy-policy',
                'intro' => 'Welcome to Executive\'s Privacy Policy',
                'content' => '
                    1. Hi, we\'re Executive Pty Ltd (ABN 11 119 159 741) and welcome to our privacy policy. 
                    This policy sets out how we handle personal information and applies across all of Executive\'s businesses, including Executive Market, Executive Studio and Tuts+ (the Sites).
                    2. When we say \'we\', \'us\' or \'Executive\' it\'s because that\'s who we are and we own and run the Sites. 
                    If we say \'policy\' we\'re talking about this privacy policy. 
                    If we say \'user terms\' we\'re talking about the rules for using each of the Sites. 
                    Take the time to read this policy because it forms a part of the user terms for each of our Sites. 
                    By using any of our Sites, you agree to this policy. 
                    If you do not agree to this policy we won\'t take it personally, but please do not use our Sites.',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Terms of use',
                'alias' => 'terms-of-use',
                'intro' => '
                    Hi, we’re Executive. We’ve met before over at Executive Market Terms. 
                    It’s good to meet you again and we’re really glad you’re keen on becoming an author with us.',
                'content' => '
                    Becoming an author is easy and if you’ve reached this point then you’re already a member and you’ve accepted our Executive Market Terms.
                    These additional terms for authors (‘Author Terms’) are an extra part of the Executive Market Terms that apply specifically to authors on the Executive Market.
                    You’ll need to agree to them before you can become an author.
                    These govern the relationships, rights and obligations of authors to us and buyers using Executive Market.
                    You agree to put your items on Executive Market and make them available to buyers on the basis stated in the Executive Market Terms and these Author Terms.',
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);
    }
}
