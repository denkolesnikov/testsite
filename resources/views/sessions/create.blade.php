@extends('template');

@section('content')

        <div class="col-md-5">
            <form action="/sessions" method="post" class="form-horizontal">

                @include('embed.errors')

                {{csrf_field()}}

                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" name="email" id="email" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" class="form-control" required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-default">Sign in</button>
                </div>

            </form>
        </div>

@endsection