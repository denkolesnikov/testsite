<h1 class="my-4">Shop Name</h1>
<div class="list-group text-center">
    <a href="/" class="list-group-item">all</a>
    @foreach($categories as $category)
        <a href="/categories/{{ $category['alias'] }}" class="list-group-item">{{ $category['title'] }}</a>
    @endforeach
</div>
