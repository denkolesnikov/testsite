@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Categories</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="col-lg-5">
    <div class="panel panel-default">
        <div class="panel-heading">
            <a class="btn btn-success" href="/admin/categories/create" role="button">+ add</a>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Alias</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($categories as $key=>$category)
                        <tr>
                            <td>{{ $key }}</td>
                            <td>{{ $category['title'] }}</td>
                            <td>{{ $category['alias'] }}</td>
                            <td>
                                <a href="/admin/categories/{{$category['alias']}}/edit"><i class="fa fa-pencil"></i></a>
                            </td>
                            <td>
                                <a href="/admin/categories/{{$category['alias']}}/delete"><i class="fa fa-trash text-danger"></i></a>
                            </td>
                        </tr>

                    @endforeach

                    {{--              <div class="col-xs-1">
                            <ul>
                                <a class="btn btn-default" href="/admin/categories/{{$category['alias']}}/edit" role="button">{{ $category['title'] }}</a>
                            </ul>
                        </div>--}}
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
    </div>
    </div>


@endsection
