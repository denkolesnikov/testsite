<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>SB Admin 2 - Bootstrap Admin Theme</title>

<!-- Bootstrap Core CSS -->
<link href="{{ asset('css/admin/bootstrap.css') }}" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="{{ asset('css/admin/metisMenu.min.css') }}" rel="stylesheet">

<!-- Custom CSS -->
<link href="{{ asset('css/admin/sb-admin-2.css') }}" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="{{ asset('css/admin/morris.css') }}" rel="stylesheet">

<!-- Custom Fonts -->
<link href="{{ asset('css/admin/font-awesome.css') }}" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<style>

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 5%;
        left: 85%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
    }

    .container123:hover .image {
        opacity: 0.3;
    }

    .container123:hover .middle {
        opacity: 1;
    }

    .text12 {
        color: #ff0406;
        font-size: 20pt;
        padding: 16px 32px;
    }
</style>