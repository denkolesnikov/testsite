<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('js/admin/bootstrap.js') }}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{ asset('js/admin/metisMenu.min.js') }}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{ asset('js/admin/raphael.min.js') }}"></script>
<script src="{{ asset('js/admin/morris.min.js') }}"></script>
{{--<script src="{{ asset('js/admin/morris-data.js') }}"></script>--}}

<!-- Custom Theme JavaScript -->
<script src="{{ asset('js/admin/sb-admin-2.js') }}"></script>