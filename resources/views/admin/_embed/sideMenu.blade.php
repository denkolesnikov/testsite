<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-shopping-cart fa-fw"></i> Products<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/admin/products">all</a>
                    </li>

                    @if(isset($categories))
                        @foreach($categories as $category)

                            <li>
                                <a href="/admin/products/category/{{$category['alias']}}">{{ $category['title'] }}</a>
                            </li>

                        @endforeach
                    @endif

                    <li>
                        <a href="/admin/products/create">add product</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-book fa-fw"></i> Pages<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/admin/pages">all</a>
                    </li>
                    <li>
                        <a href="/admin/pages/create">add page</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/admin/categories"><i class="fa fa-th-list fa-fw"></i> Categories</a>
            </li>

        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->