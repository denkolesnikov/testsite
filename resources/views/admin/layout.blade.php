<!DOCTYPE html>
<html lang="en">

<head>

    @include('admin._embed.headerConnect')

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">


        @include('admin._embed.header')

        @include('admin._embed.sideMenu')

    </nav>

    <div id="page-wrapper">

        @yield('content')

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

@include('admin._embed.bottomConnect')

</body>

</html>