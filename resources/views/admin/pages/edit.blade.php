@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{$page->title}}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="col-lg-8 mt-1">
        <form action="/admin/pages/{{$page->alias}}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            {{method_field('PUT')}}

            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" class="form-control" value="{{$page->title}}">
            </div>
            <div class="form-group">
                <label for="alias">Alias:</label>
                <input type="text" name="alias" id="alias" class="form-control" value="{{$page->alias}}">
            </div>
            <div class="form-group">
                <label for="intro">Intro:</label>
                <textarea name="intro" id="intro" class="form-control" rows="5">{{$page->intro}}</textarea>
            </div>
            <div class="form-group">
                <label for="content">Description:</label>
                <textarea name="content" id="content" class="form-control" rows="5">{{$page->content}}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-default">UPDATE</button>
            </div>

        </form>
    </div>

@endsection