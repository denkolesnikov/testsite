@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add a new page</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="col-lg-8 mt-1">
        <form action="/admin/pages" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="alias">Alias:</label>
                <input type="text" name="alias" id="alias" class="form-control">
            </div>
            <div class="form-group">
                <label for="intro">Intro:</label>
                <textarea name="intro" id="intro" class="form-control" rows="5"></textarea>
            </div>
            <div class="form-group">
                <label for="content">Description:</label>
                <textarea name="content" id="content" class="form-control" rows="5"></textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-default">CREATE</button>
            </div>

        </form>
    </div>

@endsection