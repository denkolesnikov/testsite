@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Delete page</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->


    <div class="col-md-12 mt-5">
        <form action="/admin/pages/{{$page->alias}}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            {{method_field('DELETE')}}

            <div class="form-group">
                <h4>Are you sure that you want to delete {{$page->title}}?</h4>
            </div>

            <div class="form-group">
                <button class="btn btn-danger">DELETE</button>
            </div>

        </form>
    </div>

@endsection