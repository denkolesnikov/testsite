@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Pages</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    @foreach($pages as $page)

        <div class="col-lg-4">
            <h2>{{ $page['title'] }}</h2>
            <p>{{ $page['intro'] }} </p>
            <p><a class="btn btn-primary" href="/admin/pages/{{$page['alias']}}/edit" role="button">View details »</a></p>
            <p><a class="btn btn-danger" href="/admin/pages/{{$page['alias']}}/delete" role="button">delete</a></p>
        </div>

    @endforeach

@endsection
