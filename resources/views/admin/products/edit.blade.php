@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><b>{{$product->title}}</b></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="col-lg-6 mt-1">
        <form action="/products/{{$product->alias}}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            {{method_field('PUT')}}

            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" id="title" class="form-control" value="{{$product->title}}">
            </div>
            <div class="form-group">
                <label for="alias">Alias:</label>
                <input type="text" name="alias" id="alias" class="form-control" value="{{$product->alias}}">
            </div>
            <div class="form-group">
                <label>Category</label>
                <select name="category_id" class="form-control">
                    <option value="0">none</option>
                    @foreach($categories as $category)
                        <option value="{{ $category['id'] }}" @if($category['id'] == $product->category_id) selected @endif>{{ $category['title'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="price">Price:</label>
                <input type="number" name="price" id="price" class="form-control" value="{{$product->price}}">
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea name="description" id="description" class="form-control" rows="5">{{$product->description}}</textarea>

            </div>

            <div class="form-group">
                <button class="btn btn-default">UPDATE</button>
            </div>

        </form>
    </div>

@endsection