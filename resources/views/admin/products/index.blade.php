@extends('admin.layout')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Products</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div class="row">

        @foreach($products as $product)

            <div class="col-lg-3">
                <div class="panel">
                <div class="card h-100 container123">
                    <a href="#"><img class="card-img-top" src="http://placehold.it/285x162" alt=""></a>
                    <div class="middle">
                        <a href="/admin/products/{{$product['alias']}}/delete" class=" text12"><i class="fa fa-times text-danger"></i></a>
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">
                            <a href="#">{{ $product['title'] }}</a>
                        </h4>
                        <h5>${{ $product['price'] }}</h5>
                        <p class="card-text">{{ str_limit( $product['description'] , 82) }}</p>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">
                            <a class="btn btn-outline btn-primary" href="/admin/products/{{ $product['alias'] }}/edit" role="button">View details</a>&nbsp; &#9733; &#9733; &#9733; &#9733; &#9734;
                        </small>
                    </div>
                </div>
                </div>
            </div>

        @endforeach

    </div>

@endsection