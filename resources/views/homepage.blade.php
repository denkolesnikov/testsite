@extends('template')

@section('content')

    <div class="col-lg-2">

        @include('homepage.sideMenu')

    </div>

    <div class="col-lg-10">

        @include('homepage.slider')

        <div class="row">

           {{-- @if(isset($category))

                {{$category['title']}}

                @foreach($category->products as $product)
                    <li class="list-group-item">
                        {{ $product->title }}
                    </li>
                @endforeach


            @else--}}

                @foreach($products as $product)

                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="card h-100">
                            <a href="#"><img class="card-img-top" src="http://placehold.it/285x162" alt=""></a>
                            <div class="card-body">
                                <h4 class="card-title">
                                    <a href="#">{{ $product['title'] }}</a>
                                </h4>
                                <h5>${{ $product['price'] }}</h5>
                                <p class="card-text">{{ str_limit( $product['description'] , 82) }}</p>
                            </div>
                            <div class="card-footer">
                                <small class="text-muted">
                                    <a class="btn btn-primary" href="/products/{{ $product['alias'] }}" role="button">View details</a>&nbsp; &#9733; &#9733; &#9733; &#9733; &#9734;
                                </small>
                            </div>
                        </div>
                    </div>

                @endforeach

       {{--     @endif
--}}
        </div>

    </div>

@endsection

