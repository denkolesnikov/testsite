@extends('template')

@section('content')

    <div class="container mt-4">
        <h1 class="display-4">{{ $page['title'] }}</h1>

        <p>{{ $page['intro'] }}</p>
    </div>

    <div class="col-lg-10 mt-4 mb-5">
        <p>{{ $page['content'] }} </p>
    </div>

@endsection