@extends('template')

@section('content')

    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Welcome to Pages</h1>
            <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
        </div>
    </div>

    @foreach($pages as $page)

        <div class="col-lg-4">
            <h2>{{ $page['title'] }}</h2>
            <p>{{ $page['intro'] }} </p>
            <p><a class="btn btn-primary" href="/pages/{{$page['alias']}}" role="button">View details »</a></p>
        </div>

    @endforeach

@endsection
